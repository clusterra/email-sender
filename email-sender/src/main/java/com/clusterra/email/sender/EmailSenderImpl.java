/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.email.sender;

import com.clusterra.email.infrastructure.EmailGateway;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Denis Kuchugurov
 * on 09/10/14 00:09.
 */
public class EmailSenderImpl implements EmailSender {


    @Autowired
    private EmailGateway emailGateway;

    private String fromEmail;


    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public void send(String toEmail, String subject, String body) {
        Validate.notEmpty(fromEmail);
        emailGateway.send(fromEmail, toEmail, subject, body);
    }

}
