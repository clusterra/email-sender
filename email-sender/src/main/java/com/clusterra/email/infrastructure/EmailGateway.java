/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.email.infrastructure;

import org.springframework.integration.mail.MailHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

/**
 * Created by Denis Kuchugurov
 * on 08/10/14 00:09.
 */
public interface EmailGateway {


    void send(
            @Header(MailHeaders.FROM) String fromEmail,
            @Header(MailHeaders.TO) String toEmail,
            @Header(MailHeaders.SUBJECT) String subject,
            @Payload String body);

}
