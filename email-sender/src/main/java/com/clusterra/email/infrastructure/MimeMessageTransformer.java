/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.email.infrastructure;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.mail.MailHeaders;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Denis Kuchugurov
 *         Date: 30.10.13
 */
public class MimeMessageTransformer {

    private static final Pattern IMAGE_PATH_PATTERN = Pattern.compile("src\\s*=\"cid:([\\/a-zA-Z0-9._-]+)\".*");

    @Autowired
    private JavaMailSender mailSender;

    @Transformer
    public Object transform(@Header(MailHeaders.FROM) String fromEmail, @Header(MailHeaders.TO) String toEmail,
                            @Header(MailHeaders.SUBJECT) String subject, @Payload String payload) throws Exception {
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
        mimeMessage.setFrom(new InternetAddress(fromEmail));
        mimeMessage.setSubject(MimeUtility.encodeText(subject, "UTF-8", "Q"));
        List<String> imageList = getContentImages(payload);

        if (!imageList.isEmpty()) {
            MimeMultipart multipart = new MimeMultipart("related");

            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(payload, "text/html; charset=utf-8");
            multipart.addBodyPart(messageBodyPart);

            for (String imageId : imageList) {
                messageBodyPart = new MimeBodyPart();
                DataSource fds = new ByteArrayDataSource(getImage(imageId), "image/png");
                messageBodyPart.setDataHandler(new DataHandler(fds));
                messageBodyPart.setHeader("Content-ID", "<" + imageId + ">");
                messageBodyPart.setDisposition(MimeBodyPart.INLINE);
                multipart.addBodyPart(messageBodyPart);
            }

            mimeMessage.setContent(multipart);
        } else {
            mimeMessage.setContent(payload, "text/html; charset=utf-8");
        }

        return mimeMessage;
    }

    private List<String> getContentImages(String content) {
        List<String> imageIds = new ArrayList<>();
        Matcher matcher = IMAGE_PATH_PATTERN.matcher(content);

        boolean matchFound = matcher.find();

        if (matchFound) {
            for (int i = 1; i <= matcher.groupCount(); i++) {
                imageIds.add(matcher.group(i));
            }
        }

        return imageIds;
    }

    private byte[] getImage(String imagePath) {
        ClassPathResource classPathResource = new ClassPathResource(imagePath);
        try {
            return IOUtils.toByteArray(classPathResource.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
